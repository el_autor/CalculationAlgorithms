#include "main.h"

using namespace std;

void greeting(ostream& stream)
{
    stream << "Программа для многомерной интерполяции методом Ньютона" << endl;
}

void show_menu(ostream& stream)
{
    stream << "1 - считать данные из таблицы (x, y, z)" << endl
           << "2 - ввести n(x)" << endl
           << "3 - ввести n(y)" << endl
           << "4 - получить z = f(x, y)" << endl
           << "5 - показать список команд" << endl
           << "6 - выйти из программы" << endl;
}

int main(int argc, char **argv)
{
    bool process = true, dataIsInputed = false, xGradeIsInputed = false, yGradeIsInputed = false;
    int command = 0, polynomGradeX = 0, polynomGradeY = 0;
    double x = 0, y = 0;
    istream &instream = cin;
    ostream &outstream = cout;
    vector<vector<double>> pointsTable;

    greeting(outstream);
    show_menu(outstream);

    while (process)
    {
        outstream << "Введите номер команды" << endl;
        instream >> command;

        switch (command - 1)
        {
            case (Commands::GET_DATA):

                clear(pointsTable);

                if (!get_data(pointsTable, argv[1], cout))
                {
                    return EXIT_FAILURE;
                }

                dataIsInputed = true;

                break;

            case (Commands::ENTER_NX):

                outstream << "Введите n(x):";
                instream >> polynomGradeX;
                xGradeIsInputed = true;

                break;

            case (Commands::ENTER_NY):

                outstream << "Введите n(y):";
                instream >> polynomGradeY;
                yGradeIsInputed = true;
    
                break;

            case (Commands::CALC_VALUE):

                if (!dataIsInputed || !xGradeIsInputed || !yGradeIsInputed)
                {
                    outstream << "Введите все исходные данные!" << endl;
                }
                else
                {
                    outstream << "Введите пару (x, y) через пробел:";
                    instream >> x >> y;
                    outstream << interpolate_value(pointsTable, x, y, polynomGradeX, polynomGradeY) << endl;
                }

                break;

            case (Commands::SHOW_COMMANDS):

                show_menu(outstream);

                break;

            case (Commands::QUIT):

                process = false;

                break;
            
            default:

                outstream << "Такой команды нет!" << endl;
        }
    }

    return EXIT_SUCCESS;
}