#pragma once

#include "data.h"

struct PlanePoint
{
    PlanePoint(const double& arg1, const double& arg2)
    {
        _arg1_ = arg1;
        _arg2_ = arg2;
    }

    double _arg1_;
    double _arg2_;
};  

double interpolate_value(const vector<vector<double>>& pointsTable, const double& x, const double& y, const int& gradeX, const int& gradeY);