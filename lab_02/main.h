#pragma once

#include <iostream>
#include <vector>
#include "data.h"
#include "calc.h"

enum Commands
{
    GET_DATA,
    ENTER_NX,
    ENTER_NY,
    CALC_VALUE,
    SHOW_COMMANDS,
    QUIT,
};