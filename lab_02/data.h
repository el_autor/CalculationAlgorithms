#pragma once

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>

using namespace std;

bool get_data(vector<vector<double>>& data, const string& filename, ostream& stream);
void clear(vector<vector<double>>& data);