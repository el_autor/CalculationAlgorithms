#include "data.h"

bool get_data(vector<vector<double>>& pointsTable, const string& filename, ostream& stream)
{
    ifstream datafile;
    stringstream stream_line;
    string line;
    pointsTable.push_back(vector<double>());
    double value = 0;
    int index = 0;

    datafile.open(filename);

    if (!datafile)
    {
        return false;
    }

    // Левый верхний элемент не учитывается
    pointsTable[index].push_back(0);
    getline(datafile, line);
    stream_line << line;

    while (stream_line >> value)
    {
        pointsTable[index].push_back(value);
    }

    stream_line.clear();

    while (getline(datafile, line))
    {
        index++;
        pointsTable.push_back(vector<double>());
        stream_line.clear();
        stream_line << line;

        for (unsigned i = 0; i < pointsTable[0].size(); i++)
        {
            stream_line >> value;
            pointsTable[index].push_back(value);
        }
    }

    for (unsigned i = 0; i < pointsTable.size(); i++)
    {
        for (unsigned j = 0; j < pointsTable[i].size(); j++)
        {
            stream << fixed << setprecision(3) << setw(10) << left << pointsTable[i][j] << " ";
        }

        stream << endl;
    }

    return true;
}

void clear(vector<vector<double>>& data)
{
    for (unsigned i = 0; i < data.size(); i++)
    {
        data[i].clear();
    }

    data.clear();
}