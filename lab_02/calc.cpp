#include "calc.h"

double one_dim_interpolation(vector<PlanePoint>& data, const vector<unsigned>& indexes, const double& arg)
{
    vector<vector<double>> separated_diffs;
    vector<vector<double>> arg_diff_matrix;
    double return_value = 0;
    int polynom_grade = indexes.size() - 1;

    for (unsigned i = 0; i < indexes.size(); i++)
    {
        arg_diff_matrix.push_back(vector<double>());
        for (unsigned j = 0; j < indexes.size(); j++)
        {
            arg_diff_matrix[i].push_back(data[indexes[i]]._arg1_ - data[indexes[j]]._arg1_);
        }
    }

    // Считаем первый слолбец разделенных разностей, используя ближайшие к заданному аргументу точки
    separated_diffs.push_back(vector<double>());

    for (unsigned i = 0; i < indexes.size() - 1; i++)
    {
        separated_diffs[0].push_back((data[indexes[i]]._arg2_ - data[indexes[i + 1]]._arg2_) / (data[indexes[i]]._arg1_ - data[indexes[i + 1]]._arg1_));
    }

    // Считаем остальные столбцы
    for (int i = 1; i < polynom_grade; i++)
    {
        separated_diffs.push_back(vector<double>());

        for (int j = 0, k = 0; j < polynom_grade - i; j++, k++)
        {
            separated_diffs[i].push_back((separated_diffs[i - 1][j] - separated_diffs[i - 1][j + 1]) / arg_diff_matrix[k][k + i + 1]);
        }
    }

    return_value = data[indexes[0]]._arg2_;

    // Подставляем все разности в многочлен и считаем значение
    for (int i = 0; i < polynom_grade; i++)
    {
        double local = arg - data[indexes[0]]._arg1_;

        for (int j = 0; j < i; j++)
        {
            local *= (arg - data[indexes[j + 1]]._arg1_);
        }

        local *= separated_diffs[i][0];
        return_value += local;
    }

    return return_value;
}

vector<unsigned> get_closest(const vector<PlanePoint>& data, const int& grade, const double& arg)
{
    int down_index = data.size() - 1, up_index = 0;
    bool interpolation = false;
    vector<unsigned> indexes;

    for (unsigned i = 0; i < data.size() - 1; i++)
    {
        if (data[i]._arg1_ <= arg && arg <= data[i + 1]._arg1_)
        {
            interpolation = true;
            down_index = i;
            up_index = i + 1;
            break;
        }
    }

    if (interpolation)
    {
        int counter = 0;

        while (counter != grade + 1)
        {
            if (down_index >= 0 && ((arg - data[down_index]._arg1_) <= (data[up_index]._arg1_ - arg) || up_index == static_cast<int>(data.size())))
            {
                indexes.insert(begin(indexes), down_index);
                down_index--;
                counter++;
            }

            if (counter == grade + 1)
            {
                break;
            }

            if (up_index <= static_cast<int>(data.size() - 1))
            {
                indexes.push_back(up_index);
                up_index++;
                counter++;
            }
        }
    }
    else
    {
        if (arg < data[up_index]._arg1_)
        {
            for (int i = up_index; i < grade + 1; i++)
            {
                indexes.push_back(i);
            }
        }
        else
        {
            for (int i = down_index; i > down_index - grade - 1; i--)
            {
                indexes.insert(begin(indexes), i);
            }
        } 
    }

    return indexes;
}

double interpolate_value(const vector<vector<double>>& pointsTable, const double& x, const double& y, const int& gradeX, const int& gradeY)
{
    vector<unsigned> closestLinesX;
    vector<unsigned> closestLinesY;
    vector<PlanePoint> local;

    for (unsigned i = 1; i < pointsTable.size(); i++)
    {   
        local.push_back(PlanePoint(pointsTable[i][0], 0));
    }

    // тут вставить сортировку
    closestLinesX = get_closest(local, gradeX, x);

    for (unsigned i = 0; i < closestLinesX.size(); i++)
    {
        closestLinesX[i]++;
        //cout << closestLinesX[i] << " ";
    }

    //cout << endl << endl;

    local.clear();

    for (unsigned j = 1; j < pointsTable[0].size(); j++)
    {
        local.push_back(PlanePoint(pointsTable[0][j], 0));
    }

    closestLinesY = get_closest(local, gradeY, y); 

    for (unsigned i = 0; i < closestLinesY.size(); i++)
    {
        closestLinesY[i]++;
        //cout << closestLinesY[i] << " ";
    }

    //cout << endl;

    vector<PlanePoint> last_line_y;

    for (unsigned i = 0; i < pointsTable.size(); i++)
    {
        vector<PlanePoint> line;
        
        for (unsigned j = 0; j < pointsTable[0].size(); j++)
        {
            // точки (y, z) для одномерной интерполяции
            line.push_back(PlanePoint(pointsTable[0][j], pointsTable[i][j]));
        }

        last_line_y.push_back(PlanePoint(pointsTable[i][0], one_dim_interpolation(line, closestLinesY, y)));
    }

    /*
    for (unsigned i = 0; i < last_line_y.size(); i++)
    {
        cout << last_line_y[i]._arg1_ << " " << last_line_y[i]._arg2_ << endl;
    }
    */

    return one_dim_interpolation(last_line_y, closestLinesX, x);
}