#include <iostream>
#include <fstream>
#include <vector>
#include "commands.h"
#include "data.h"
#include "calc.h"

void greeting(std::ostream& stream)
{
    stream << "Программа для интерполяции методом наименьших квадратов" << std::endl;
}

void showMenu(std::ostream& stream)
{
    stream << "1 - ввести данные из файла" << std::endl
           << "2 - показать таблицу с точками" << std::endl
           << "3 - поменять веса" << std::endl
           << "4 - ввести степень полинома" << std::endl
           << "5 - рассчитать значение функции" << std::endl
           << "6 - графический анализ" << std::endl 
           << "7 - показать команды меню" << std::endl
           << "8 - выйти из программы" << std::endl;
}

int main(int argc, char** argv)
{
    std::vector<Point> basePoints;
    bool process = true, dataIsInputed = false, gradeIsInputed = false;
    double xFind = 0, yFind = 0;
    int command = 0, polynomGrade = 0;
    std::istream &instream = std::cin;
    std::ostream &outstream = std::cout;

    greeting(outstream);
    showMenu(outstream);

    while (process)
    {
        outstream << "Введите номер команды:" << std::endl; 
        instream >> command;

        switch (command - 1)
        {
            case (Commands::GET_DATA):

                basePoints.clear();
                inputData(basePoints, argv[1]);
                dataIsInputed = true;

                break;

            case (Commands::SHOW_DATA):

                if (dataIsInputed)
                {
                    showData(outstream, basePoints);
                }
                else
                {
                    outstream << "Не введены данные!" << std::endl;
                }

                break;

            case (Commands::CHANGE_WEIGHTS):

                if (dataIsInputed)
                {
                    changeWeight(instream, outstream, basePoints);
                }
                else
                {
                    outstream << "Не введены данные!" << std::endl;
                }

                break;

            case (Commands::INPUT_GRADE):

                gradeIsInputed = true;
                inputGrade(instream, outstream, polynomGrade);

                break;

            case (Commands::CALC_VALUE):

                if (!dataIsInputed)
                {
                    outstream << "Не введены данные!" << std::endl;
                }
                else if (!gradeIsInputed)
                {
                    outstream << "Не введена степень полинома!" << std::endl;
                }
                else
                {
                    outstream << "Введите x:" << std::endl;
                    instream >> xFind;
                    outstream << (yFind = getValue(basePoints, polynomGrade, xFind)) << std::endl;
                }

                break;

            case (Commands::SHOW_GRAPHS):

                if (dataIsInputed)
                {
                    system("./clear_data.sh");
                    graphAnalysis(instream, outstream, basePoints);
                    system("./graph.sh");
                }
                else
                {
                    outstream << "Не введены данные!" << std::endl;
                }

                break;

            case (Commands::SHOW_MENU):                

                showMenu(outstream);

                break;
        
            case (Commands::EXIT):

                process = false;

                break;    
        }
    }

    return EXIT_SUCCESS;
}