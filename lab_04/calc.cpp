#include "calc.h"

std::vector<std::vector<double>> initMatrix(const int& rows, const int& cols)
{
    std::vector<std::vector<double>> matrix;

    for (int i = 0; i < rows; i++)
    {
        matrix.push_back(std::vector<double>(cols, 0));
    }

    return matrix;
}

double calcCoeff(const std::vector<Point>& points, const int& indexI, const int& indexJ)
{
    double element = 0;
    int pointsTotal = static_cast<int>(points.size());

    for (int i = 0; i < pointsTotal; i++)
    {
        element += points[i].w * pow(points[i].x, indexI + indexJ);
    }

    return element;
}

void fillCoeffs(std::vector<std::vector<double>>& matrix, const std::vector<Point>& points)
{   
    int matrixSize = static_cast<int>(matrix.size());

    for (int i = 0; i < matrixSize; i++)
    {
        for (int j = 0; j < matrixSize; j++)
        {
            matrix[i][j] = calcCoeff(points, i, j);
        }
    }
}

double calcResult(const std::vector<Point>& points, const int& indexI)
{
    double element = 0;
    int pointsTotal = static_cast<int>(points.size());

    for (int i = 0; i < pointsTotal; i++)
    {
        element += points[i].w * points[i].y * pow(points[i].x, indexI);
    }

    return element;
}

void fillResult(std::vector<std::vector<double>>& matrix, const std::vector<Point>& points)
{
    int matrixSize = static_cast<int>(matrix.size());

    for (int i = 0; i < matrixSize; i++)
    {
        matrix[i][0] = calcResult(points, i);
    }
}

void addRow(std::vector<std::vector<double>>& matrix, const int& from, const int& to)
{
    int size = static_cast<int>(matrix.size());

    for (int i = 0; i < size; i++)
    {
        matrix[to][i] += matrix[from][i];
    }
}

double getDeterminant(std::vector<std::vector<double>> matrix)
{
    int size = static_cast<int>(matrix.size());
    double det = 1, mult = 0;
    int j = 0;

    //std::cout << size << std::endl;

    for (int i = 0; i < size; i++)
    {
        j = 0;
        
        while (j < size && matrix[j++][i] == 0);
       
        //std::cout << "j - " << j << std::endl;

        if (j-- == size)
        {
            return 0;
        }

        //std::cout << "j - " << j << std::endl;

        for (int k = i; k < size; k++) // Убираем нулевые элементы в столбце
        {
            if (matrix[k][i] == 0)
            {
                addRow(matrix, j, k);
            }
        }

        //std::cout << "here" << std::endl;

        for (j = i + 1; j < size; j++)
        {
            mult = -matrix[j][i] / matrix[i][i];

            for (int k = 0; k < size; k++)
            {
                matrix[j][k] += matrix[i][k] * mult;
            }
        }


        /*
        for (unsigned i = 0; i < matrix.size(); i++)
        {
            for (unsigned j = 0; j < matrix.size(); j++)
            {
                std::cout << matrix[i][j] << " ";
            }   

            std::cout << std::endl;
        }
        */


        det *= matrix[i][i];
    }

    return det;
}

void changeCols(std::vector<std::vector<double>>& firstMatrix, const int& indexFirst,
                std::vector<std::vector<double>>& secondMatrix, const int& indexSecond)
{
    int size = static_cast<int>(firstMatrix.size());
    double temp = 0;

    for (int i = 0; i < size; i++)
    {
        temp = firstMatrix[i][indexFirst];
        firstMatrix[i][indexFirst] = secondMatrix[i][indexSecond];
        secondMatrix[i][indexSecond] = temp;
    }
}

double getValue(const std::vector<Point>& points, const int& grade, const double& x)
{
    std::vector<std::vector<double>> coeffMatrix = initMatrix(grade + 1, grade + 1), resultMatrix = initMatrix(grade + 1, 1);
    std::vector<double> roots;
    double result = 0, det = 0, detA = 0;

    fillCoeffs(coeffMatrix, points);
    fillResult(resultMatrix, points);

    /*for (unsigned i = 0; i < coeffMatrix.size(); i++)
    {
        for (unsigned j = 0; j < coeffMatrix.size(); j++)
        {
            std::cout << coeffMatrix[i][j] << " ";
        }

        std::cout << std::endl;
    }

    std::cout << std::endl;

    for (unsigned i = 0; i < resultMatrix.size(); i++)
    {
        std::cout << resultMatrix[i][0] << std::endl;
    }*/

    det = getDeterminant(coeffMatrix);

    //std::cout << "here" << std::endl;
    
    for (int i = 0; i <= grade; i++)
    {
        changeCols(coeffMatrix, i, resultMatrix, 0);
        detA = getDeterminant(coeffMatrix);
        changeCols(coeffMatrix, i, resultMatrix, 0);
        result += (detA / det) * pow(x, i);
    }

    return result;
}

void graphAnalysis(std::istream& instream, std::ostream& outstream, const std::vector<Point>& basePoints)
{
    std::vector<int> grades;
    double startX = basePoints[0].x, endX = basePoints[basePoints.size() - 1].x, y = 0;
    int grade = 0, gradesSize = 0;
    std::ofstream outFile;

    outstream << "Введите степени полинома, которые будут отображены на графике(для завершения ввода введите \"-1\"):";
    instream >> grade;

    while (grade != -1)
    {
        grades.push_back(grade);
        instream >> grade;
    }

    gradesSize = static_cast<int>(grades.size());

    outFile.open("./graph_data/grades.txt");

    for (int i = 0; i < gradesSize; i++)
    {
        outFile << grades[i] << std::endl;
    }

    outFile.close();

    for (int i = 0; i < gradesSize; i++)
    {
        outFile.open("./graph_data/data_" + std::to_string(grades[i]) + ".txt");

        for (double x = startX; x <= endX; x += 0.01)
        {
            y = getValue(basePoints, grades[i], x);
            outFile << x << " " << y << std::endl;
        }

        outFile.close();
    }
}