#pragma once

#include <vector>
#include <cmath>
#include <fstream>
#include "data.h"

double getValue(const std::vector<Point>& points, const int& grade, const double& x);
void graphAnalysis(std::istream& instream, std::ostream& outstream, const std::vector<Point>& basePoints);