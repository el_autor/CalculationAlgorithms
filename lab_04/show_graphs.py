import matplotlib.pyplot as plt

window = plt.figure(0)
window.canvas.set_window_title('Графический анализ')
plt.title("Аппроксимация")

allGradesFile = open("./graph_data/grades.txt", "r")

grades = allGradesFile.readlines()

xSrc = []
ySrc = []
sourcePointsFile = open("./init_data/data.txt", "r")

for line in sourcePointsFile:
    point = line[:-1].split(" ")
    xSrc.append(float(point[0]))
    ySrc.append(float(point[1]))

sourcePointsFile.close()

plt.scatter(xSrc, ySrc)

for i in range(len(grades)):
    grades[i] = grades[i][:-1]

    dataFile = open("./graph_data/data_" + grades[i] + ".txt", "r")
    x = []
    y = []

    for line in dataFile:
        point = line[:-1].split(" ")
        x.append(float(point[0]))
        y.append(float(point[1]))

    plt.plot(x, y, label = "n = " + grades[i])
    dataFile.close()

allGradesFile.close()

plt.legend()
plt.show()