#pragma once

enum Commands
{
    GET_DATA,
    SHOW_DATA,
    CHANGE_WEIGHTS,
    INPUT_GRADE,
    CALC_VALUE,
    SHOW_GRAPHS,
    SHOW_MENU,
    EXIT,
};