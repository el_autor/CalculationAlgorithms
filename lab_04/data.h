#pragma once

#include <vector>
#include <string>
#include <fstream>

struct Point
{
    double x = 0, y = 0, w = 0;
};

void inputData(std::vector<Point>& points, const std::string& filename);
void showData(std::ostream& stream, const std::vector<Point>& points);
void changeWeight(std::istream& instream, std::ostream& outstream, std::vector<Point>& points);
void inputGrade(std::istream& instream, std::ostream& outstream, int& grade);