#include "data.h"

bool operator >>(std::ifstream& stream, Point& point)
{
    return static_cast<bool>(stream >> point.x >> point.y >> point.w);
}

void inputData(std::vector<Point>& points, const std::string& filename)
{
    std::ifstream sourceFile;
    Point localPoint;

    sourceFile.open(filename);

    while (sourceFile >> localPoint)
    {
        points.push_back(localPoint);
    }

    sourceFile.close();
}

void showData(std::ostream& stream, const std::vector<Point>& points)
{
    int size = static_cast<int>(points.size());

    for (int i = 0; i < size; i++)
    {
        stream << points[i].x << " " << points[i].y << " " << points[i].w << std::endl;
    }   
}

void changeWeight(std::istream& instream, std::ostream& outstream, std::vector<Point>& points)
{   
    int pointIndex = 0, pointsSize = static_cast<int>(points.size());
    double newWeight = 0;
    std::ofstream sourceFile; 

    outstream << "Введите номер точки:" << std::endl;
    instream >> pointIndex;
    outstream << "Введите новый вес:" << std::endl;
    instream >> newWeight;

    points[pointIndex - 1].w = newWeight;

    sourceFile.open("./init_data/data.txt");

    for (int i = 0; i < pointsSize; i++)
    {
        sourceFile << points[i].x << " " << points[i].y << " " << points[i].w << std::endl;
    }

    sourceFile.close();
}

void inputGrade(std::istream& instream, std::ostream& outstream, int& grade)
{
    outstream << "Введите степень полинома:" << std::endl;
    instream >> grade;
}