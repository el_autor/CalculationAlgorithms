#pragma once

#include <vector>
#include "data.h"

#define STEP 0.001
#define DELTA 0.1
#define EPS 0.000001
#define CALC_LIMIT 1000

using namespace std;

double get_value(vector<Point> data, const double& x, int polynom_grade);
vector <double> get_roots(vector<Point> data, const int& polynom_grade);

