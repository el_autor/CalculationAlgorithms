#include "main.h"

using namespace std;

void greeting(ostream& stream)
{
    stream << "Программа для построения интерполяционного полинома Ньютона" << endl;
}

void show_commands(ostream& stream)
{
    stream << "1 - считать табличные данные (x, y)" << endl
           << "2 - ввести степень полинома" << endl
           << "3 - ввести x" << endl
           << "4 - посчитать Pn(x)" << endl
           << "5 - найти корни методом половинного деления" << endl
           << "6 - найти корень методом обратной интерполяции" << endl
           << "7 - показать список команд" << endl
           << "8 - выйти из программы" << endl;
}

int main(int argc, char **argv)
{
    bool process = true, arg_is_inputed = false, grade_is_inputed = false, data_is_inputed = false;
    int command = 0, polynom_grade = 0;
    double x = 0;
    ostream &outstream = cout;
    istream &instream = cin;
    vector<Point> data;
    vector<double> roots;

    greeting(outstream);
    show_commands(outstream);

    while (process)
    {
        outstream << "Введите номер команды:" << endl;
        instream >> command;

        switch (command - 1)
        {
            case Command::GET_DATA:

                data.clear();
                data_is_inputed = true;

                if (get_data(data, argv[1]) != OK)
                {
                    return ERROR;
                }

                for (unsigned i = 0; i < data.size(); i++)
                {
                    outstream << fixed << data[i]._x_ << " " << data[i]._y_ << endl;
                }

                break;

            case Command::INPUT_GRADE:
                
                outstream << "Введите степень полинома:";
                instream >> polynom_grade;
                grade_is_inputed = true;

                break;

            case Command::INPUT_ARGUMENT:
                
                outstream << "Введите x:";
                instream >> x;
                arg_is_inputed = true;

                break;

            case Command::CALC_VALUE:

                if (!arg_is_inputed || !grade_is_inputed || !data_is_inputed)
                {
                    outstream << "Введите все исходные данные!" << endl;
                }
                else
                {
                    outstream << get_value(data, x, polynom_grade) << endl; 
                }

                break;

            case Command::GET_ROOTS:
                
                if (!grade_is_inputed || !data_is_inputed)
                {
                    outstream << "Введите все исходные данные!" << endl;
                }
                else
                {
                    roots = get_roots(data, polynom_grade);
                    outstream << "Корни:" << endl;

                    for (const double& root : roots)
                    {
                        outstream << root << endl;
                    }
                }

                break;

            case Command::BACK_INTERPOLATION:

                if (!grade_is_inputed || !data_is_inputed)
                {
                    outstream << "Введите все исходные данные!" << endl;
                }
                else
                {
                    outstream << get_value(replace_argument(data), 0, polynom_grade) << endl;
                }

                break;

            case Command::SHOW_LEGEND:

                show_commands(outstream);

                break;

            case Command::EXIT:

                process = false;

                break;

            default:

                outstream << "Такой команды нет!" << endl;  
        }
    }

    return OK;
}