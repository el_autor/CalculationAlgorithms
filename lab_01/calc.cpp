#include "calc.h"

#include <iostream>

static void bubble_sort(vector<Point>& data)
{
    Point temp = Point(0, 0);

    for (unsigned i = 0; i < data.size() - 1; i++)
    {
        for (unsigned j = 0; j < data.size() - 1; j++)
        {
            if (data[j]._x_ > data[j + 1]._x_)
            {   
                temp = data[j];
                data[j] = data[j + 1];
                data[j + 1] = temp;
            }
        }
    }
}

static int remove_collisions(vector<Point>& data)
{
    int collisions_counter = 0;

    for (vector<Point>::iterator it = begin(data); it < end(data) - 1; it++)
    {
        if (abs(it->_x_ - (it + 1)->_x_) <= EPS)
        {
            data.erase((it + 1));
            it--;
            collisions_counter++;
        }
    }

    return collisions_counter;
}

double get_value(vector<Point> data, const double& x, int polynom_grade)
{
    // Проверяем функцию на периодичность
    int collisions = 0, table_size = static_cast<int>(data.size());
    
    bubble_sort(data); // Сортируем аргумент по возрастванию
    collisions = remove_collisions(data);
    table_size -= collisions;

    if (polynom_grade > table_size - 1)
    {
        polynom_grade = table_size - 1;
    } 

    //cout << "show sorted" << endl;
    /*
    for (unsigned i = 0; i < data.size(); i++)
    {
        cout << data[i]._x_ << " " << data[i]._y_ << endl;
    }
    */

    int down_index = data.size() - 1, up_index = 0;
    bool interpolation = false;
    vector<unsigned> indexes;
    vector<vector<double>> separated_diffs;
    vector<vector<double>> arg_diff_matrix;
    double return_value = 0;

    for (unsigned i = 0; i < data.size() - 1; i++)
    {
        if (data[i]._x_ <= x && x <= data[i + 1]._x_)
        {
            interpolation = true;
            down_index = i;
            up_index = i + 1;
            break;
        }
    }

    //cout << "here" << endl << down_index << " " << up_index << endl;

    if (interpolation)
    {
        int counter = 0;

        while (counter != polynom_grade + 1)
        {
            /*
            for (unsigned i = 0; i < indexes.size(); i++)
            {
                cout << indexes[i] << " ";
            }
            cout << endl;
            */

            if (down_index >= 0)
            {
                indexes.insert(begin(indexes), down_index);
                down_index--;
                counter++;
            }

            if (counter == polynom_grade + 1)
            {
                break;
            }

            if (up_index <= static_cast<int>(data.size() - 1))
            {
                indexes.push_back(up_index);
                up_index++;
                counter++;
            }
        }
    }
    else
    {
        if (x < data[up_index]._x_)
        {
            for (int i = up_index; i < polynom_grade + 1; i++)
            {
                indexes.push_back(i);
            }
        }
        else
        {
            for (int i = down_index; i > down_index - polynom_grade - 1; i--)
            {
                indexes.insert(begin(indexes), i);
            }
        } 
    }

    //cout << "here1" << endl;
    /*
    for (unsigned i = 0; i < indexes.size(); i++)
    {
        cout << indexes[i] << " ";
    }
    cout << endl;
    */

    // Тут заполняем матрицу всеми возможными разностями аргументов
    for (unsigned i = 0; i < indexes.size(); i++)
    {
        arg_diff_matrix.push_back(vector<double>());
        for (unsigned j = 0; j < indexes.size(); j++)
        {
            arg_diff_matrix[i].push_back(data[indexes[i]]._x_ - data[indexes[j]]._x_);
        }
    }

    /*
    for (unsigned i = 0; i < indexes.size(); i++)
    {
        for (unsigned j = 0; j < indexes.size(); j++)
        {
            cout << arg_diff_matrix[i][j] << " ";
        }
        cout << endl;
    }
    */


    // Считаем первый слолбец разделенных разностей, используя ближайшие к заданному аргументу точки
    separated_diffs.push_back(vector<double>());
    //cout << "check" << endl;

    //cout << "diffs 1 column" << endl;
    for (unsigned i = 0; i < indexes.size() - 1; i++)
    {
        //cout << (data[indexes[i]]._y_ - data[indexes[i + 1]]._y_) / (data[indexes[i]]._x_ - data[indexes[i + 1]]._x_) << endl;  
        separated_diffs[0].push_back((data[indexes[i]]._y_ - data[indexes[i + 1]]._y_) / (data[indexes[i]]._x_ - data[indexes[i + 1]]._x_));
    }

   // cout << "here2" << endl;

    // Считаем остальные столбцы
    for (int i = 1; i < polynom_grade; i++)
    {
        separated_diffs.push_back(vector<double>());

        for (int j = 0, k = 0; j < polynom_grade - i; j++, k++)
        {
            separated_diffs[i].push_back((separated_diffs[i - 1][j] - separated_diffs[i - 1][j + 1]) / arg_diff_matrix[k][k + i + 1]);
        }
    }

    /*    
    cout << "all diffs output" << endl;
    for (unsigned i = 0; i < separated_diffs.size(); i++)
    {
        for (unsigned j = 0; j < separated_diffs[i].size(); j++)
        {
            cout << separated_diffs[i][j] << " ";
        }

        cout << endl;
    }
    */
    //cout << "here3" << endl;
    

    return_value = data[indexes[0]]._y_;
    //cout << return_value << "return value" << endl;

    // Подставляем все разности в многочлен и считаем значение
    for (int i = 0; i < polynom_grade; i++)
    {
        double local = x - data[indexes[0]]._x_;
        //cout << local << endl;

        for (int j = 0; j < i; j++)
        {
            local *= (x - data[indexes[j + 1]]._x_);
        }

        //cout << separated_diffs[i][0] << endl;

        local *= separated_diffs[i][0];
        //cout << local << endl;
        return_value += local;
    }

    return return_value;
}

vector <double> get_roots(vector<Point> data, const int& polynom_grade)
{
    vector<double> roots;
    bubble_sort(data);

    for (float start = data[0]._x_, end = start + STEP; end <= data[data.size() - 1]._x_ + STEP; start += STEP, end += STEP)
    {
        double copy_start = start, copy_end = end, middle = (start + end) / 2;
        int iterations = 0;

        while (abs(get_value(data, middle, polynom_grade)) > EPS && iterations < CALC_LIMIT)
        {
            if (get_value(data, copy_start, polynom_grade) * get_value(data, middle, polynom_grade) <= 0)
            {
                copy_end = middle;
            }
            else if (get_value(data, middle, polynom_grade) * get_value(data, copy_end, polynom_grade) <= 0)
            {
                copy_start = middle;
            }
            else
            {
                break;
            }

            middle = (copy_start + copy_end) / 2;
            iterations++;
        }

        if (abs(get_value(data, middle, polynom_grade)) <= EPS && iterations < CALC_LIMIT && middle <= data[data.size() - 1]._x_)
        {
            roots.push_back(middle);
        }
    }

    return roots;
}