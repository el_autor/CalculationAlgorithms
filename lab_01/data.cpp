#include "data.h"

#include <iostream>

int get_data(vector<Point>& data, const string& filename)
{
    ifstream datafile;
    double x = 0, y = 0;

    datafile.open(filename);

    if (!datafile)
    {
        return ERROR;
    }    

    while (datafile >> x)
    {   
        datafile >> y;
        data.push_back(Point(x, y));
    }

    return OK;
}

vector<Point> replace_argument(vector<Point> data)
{
    double temp = 0;

    for (unsigned i = 0; i < data.size(); i++)
    {
        temp = data[i]._x_;
        data[i]._x_ = data[i]._y_;
        data[i]._y_ = temp;
    }

    return data;
}