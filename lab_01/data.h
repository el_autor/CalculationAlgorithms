#pragma once

#include <fstream>
#include <vector>
#include <string>
#include "consts.h"

struct Point
{
    Point(const double& x, const double& y)
    {
        _x_ = x;
        _y_ = y;
    }

    double _x_;
    double _y_;
};  


using namespace std;

int get_data(vector<Point>& data, const string& filename);
vector<Point> replace_argument(vector<Point> data);