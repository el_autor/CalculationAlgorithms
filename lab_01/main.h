#pragma once

#include <iostream>
#include <vector>
#include "data.h"
#include "consts.h"
#include "calc.h"

enum Command
{
    GET_DATA,
    INPUT_GRADE,
    INPUT_ARGUMENT,
    CALC_VALUE,
    GET_ROOTS,
    BACK_INTERPOLATION,
    SHOW_LEGEND,
    EXIT,
};