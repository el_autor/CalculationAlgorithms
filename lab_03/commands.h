#ifndef _COMMANDS_H_

#define _COMMANDS_H_

enum Commands
{
    GET_DATA,
    GET_VALUE,
    SHOW_COMMANDS,
    QUIT,
};

#endif