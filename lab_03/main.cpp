#include <iostream>
#include <vector>
#include "data.h"
#include "calc.h"
#include "commands.h"

using namespace std;

void greeting(ostream& stream)
{
    stream << "Программа для интерполяции функции кубическим сплайном" << endl;
}

void show_menu(ostream& stream)
{
    stream << "1 - считать точки из файла (x, y)" << endl
           << "2 - получить y = f(x)" << endl
           << "3 - показать список команд" << endl
           << "4 - выйти из программы" << endl;
}

int main(int argc, char **argv)
{
    bool process = true, dataIsInputed = false;
    int command = 0;
    double xFind = 0, yFind = 0;
    istream &instream = cin;
    ostream &outstream = cout;
    vector<Point> basePoints;

    greeting(outstream);
    show_menu(outstream);

    while (process)
    {
        outstream << "Введите номер команды:" << endl;
        instream >> command;

        switch (command - 1)
        {
            case (Commands::GET_DATA):

                basePoints.clear();
                inputData(basePoints, argv[1]);
                dataIsInputed = true;

                break;

            case (Commands::GET_VALUE):

                if (!dataIsInputed)
                {
                    outstream << "Не введены точки!" << endl;
                }
                else
                {
                    outstream << "Введите значение:" << endl;
                    instream >> xFind;
                    cout << (yFind = getValue(basePoints, xFind)) << endl;
                }

                break;

            case (Commands::SHOW_COMMANDS):

                show_menu(outstream);

                break;

            case (Commands::QUIT):

                process = false;

                break;
            
            default:

                outstream << "Такой команды нет!" << endl;
        }

    }

    return EXIT_SUCCESS;
}