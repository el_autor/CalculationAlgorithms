#ifndef _CALC_H_

#define _CALC_H_

#include <iostream>
#include <algorithm>
#include <vector>
#include "data.h"

struct RunCoeffs
{
    double omega = 0, lambda = 0;    
};

struct Diffs
{
    double xDiff = 0, derivative = 0;
};

using namespace std;

double getValue(const vector<Point>& basePoints, const double& xFind);

#endif