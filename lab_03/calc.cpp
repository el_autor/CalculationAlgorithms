#include "calc.h"

vector<Diffs> calcDiffs(const vector<Point>& basePoints)
{
    vector<Diffs> diffs;
    Diffs localDiff;

    for (unsigned i = 1; i < basePoints.size(); i++)
    {
        localDiff.xDiff = basePoints[i].x - basePoints[i - 1].x;
        localDiff.derivative = (basePoints[i].y - basePoints[i - 1].y) / (localDiff.xDiff);
        diffs.push_back(localDiff);
    }

    return diffs;
}

vector<RunCoeffs> getRunningCoefs(const vector<Diffs>& allDiffs)
{
    vector<RunCoeffs> coeffs;
    RunCoeffs local;

    // Начальные прогоночные коэффициенты
    local.omega = -(allDiffs[1].xDiff) / (2 * (allDiffs[0].xDiff + allDiffs[1].xDiff));
    local.lambda = 3 * (allDiffs[1].derivative - allDiffs[0].derivative) / (2 * (allDiffs[0].xDiff + allDiffs[1].xDiff));
    coeffs.push_back(local);

    for (unsigned i = 1; i < allDiffs.size() - 1; i++)
    {
        local.omega = -allDiffs[i + 1].xDiff / (2 * allDiffs[i].xDiff + 2 * allDiffs[i + 1].xDiff + allDiffs[i].xDiff * coeffs[i - 1].omega);
        local.lambda = (3 * allDiffs[i + 1].derivative - 3 * allDiffs[i].derivative - allDiffs[i].xDiff * coeffs[i - 1].lambda) / (2 * allDiffs[i].xDiff + 2 * allDiffs[i + 1].xDiff + allDiffs[i].xDiff * coeffs[i - 1].omega);
        coeffs.push_back(local);
    }

    return coeffs;
}

vector<double> getC(const vector<RunCoeffs>& runCoeffs)
{
    vector<double> coeffsC;
    unsigned sizeCoeffs = runCoeffs.size();

    coeffsC.push_back(0);

    for (unsigned i = 1; i < sizeCoeffs + 1; i++)
    {
        coeffsC.push_back(runCoeffs[sizeCoeffs - i].omega * coeffsC[i - 1] + runCoeffs[sizeCoeffs - i].lambda);
    }

    coeffsC.push_back(0);
    reverse(begin(coeffsC), end(coeffsC));

    return coeffsC;
}

vector<double> getB(const vector<Diffs>& allDiffs, const vector<double>& coeffsC)
{
    vector<double> coeffsB;
    unsigned sizeCoeffs = coeffsC.size();

    for (unsigned i = 1; i < sizeCoeffs; i++)
    {
        coeffsB.push_back(allDiffs[i - 1].derivative + (2 * coeffsC[i] * allDiffs[i - 1].xDiff + allDiffs[i - 1].xDiff * coeffsC[i - 1]) / 3);
    }
    return coeffsB;
}


vector <double> getD(const vector<Diffs>& allDiffs, const vector<double>& coeffsC)
{
    vector<double> coeffsD;
    unsigned sizeCoeffs = coeffsC.size();

    for (unsigned i = 1; i < sizeCoeffs; i++)
    {
        coeffsD.push_back((coeffsC[i] - coeffsC[i - 1]) / (3 * allDiffs[i - 1].xDiff));
    }

    return coeffsD;
}

double calcValue(const double& xFind, const vector<double>& coeffsB, const vector<double>& coeffsC, const vector<double>& coeffsD, const vector<Point>& basePoints)
{
    double diff = 0;
    unsigned index = 0;

    for (unsigned i = 1; i < basePoints.size(); i++)
    {
        if (xFind >= basePoints[i - 1].x && xFind <= basePoints[i].x)
        {
            index = i;
            break;
        }
    }

    diff = xFind - basePoints[index].x;
    //cout << "Index" << index << endl;
    //cout << "xDiff" << diff << endl;
    return basePoints[index].y + coeffsB[index - 1] * diff + coeffsC[index] * diff * diff + coeffsD[index - 1] * diff * diff * diff; 
}

double getValue(const vector<Point>& basePoints, const double& xFind)
{
    vector<Diffs> allDiffs = calcDiffs(basePoints);
    vector<RunCoeffs> runningCoefs = getRunningCoefs(allDiffs);
    vector<double> coeffsC = getC(runningCoefs);
    vector<double> coeffsB = getB(allDiffs, coeffsC);
    vector<double> coeffsD = getD(allDiffs, coeffsC);

    /*
    cout << endl << "basePoints" << endl << endl;

    for (unsigned i = 0; i < basePoints.size(); i++)
    {
        cout << basePoints[i].x << " " << basePoints[i].y << endl;
    }

    cout << endl << "Diffs" << endl << endl;

    for (unsigned i = 0; i < allDiffs.size(); i++)
    {
        cout << allDiffs[i].xDiff << " " << allDiffs[i].derivative << endl;
    }

    cout << endl << "Running coefs" << endl << endl;

    for (unsigned i = 0; i < runningCoefs.size(); i++)
    {
        cout << runningCoefs[i].lambda << " " << runningCoefs[i].omega << endl;
    }

    cout << endl << "B" << endl << endl;

    for (unsigned i = 0; i < coeffsB.size(); i++)
    {
        cout << coeffsB[i] << endl;
    }

    cout << endl << "C" << endl << endl;

    for (unsigned i = 0; i < coeffsC.size(); i++)
    {
        cout << coeffsC[i] << endl;
    }

    cout << endl << "D" << endl << endl;

    for (unsigned i = 0; i < coeffsD.size(); i++)
    {
        cout << coeffsD[i] << endl;
    }

    cout << endl;
    */

    return calcValue(xFind, coeffsB, coeffsC, coeffsD, basePoints);
}