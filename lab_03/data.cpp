#include "data.h"

bool operator >>(ifstream& stream, Point& point)
{
    return static_cast<bool>(stream >> point.x >> point.y);
}

void inputData(vector<Point>& points, const string& filename)
{
    ifstream sourceFile;
    Point localPoint;

    sourceFile.open(filename);

    while (sourceFile >> localPoint)
    {
        points.push_back(localPoint);
    }

    sourceFile.close();
}