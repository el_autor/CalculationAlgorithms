#ifndef _DATA_H_

#define _DATA_H_

#include <iostream>
#include <vector>
#include <fstream>

struct Point
{
    double x = 0, y = 0;
};

using namespace std;

void inputData(vector<Point>& points, const string& filename);

#endif