#include "calc.h"

vector<double> getRightSided(const vector<Point>& data)
{
    vector<double> result;
    double step = data[1]._x_ - data[0]._x_;

    for (int i = 0; i < (int)data.size() - 1; i++)
    {
        result.push_back((data[i + 1]._y_ - data[i]._y_) / step);
    }

    return result;
}

vector<double> getLeftSided(const vector<Point>& data)
{
    vector<double> result;
    double step = data[1]._x_ - data[0]._x_;

    for (int i = 1; i < (int)data.size(); i++)
    {
        result.push_back((data[i]._y_ - data[i - 1]._y_) / step);
    }

    return result;
}

vector<double> getCentral(const vector<Point>& data)
{
    vector<double> result;
    double step = 2 * (data[1]._x_ - data[0]._x_);

    for (int i = 1; i < (int)data.size()- 1; i++)
    {
        result.push_back((data[i + 1]._y_ - data[i - 1]._y_) / step);
    }

    return result;
}

vector<double> getDoubleCentral(const vector<Point>& data)
{
    vector<double> result;
    double step = data[1]._x_ - data[0]._x_;

    step *= step;

    for (int i = 1; i < (int)data.size()- 1; i++)
    {
        result.push_back((data[i - 1]._y_ - 2 * data[i]._y_ + data[i + 1]._y_) / step);
    }

    return result;
}

vector<double> runge(const vector<Point>& data)
{
    vector<double> result, rightSided, anotherStep;
    double step = 2 * (data[1]._x_ - data[0]._x_);

    // использую правостороннюю производную
    rightSided = getRightSided(data);
    
    for (int i = 0; i < (int)data.size() - 2; i++)
    {
        anotherStep.push_back((data[i + 2]._y_ - data[i]._y_) / step);
    }

    for (int i = 0; i < (int)anotherStep.size(); i++)
    {
        result.push_back(rightSided[i] + (rightSided[i] - anotherStep[i]));
    }

    return result;
}

vector<double> alignVars(const vector<Point>& data)
{
    vector<Point> alignData;
    vector<double> result;

    for (int i = 0; i < (int)data.size(); i++)
    {
        alignData.push_back(Point(1 / data[i]._x_, data[i]._y_ / data[i]._x_));
    }

    for (int i = 0; i < (int)data.size() - 1; i++)
    {
        result.push_back((alignData[i + 1]._y_ - alignData[i]._y_) / (alignData[i + 1]._x_ - alignData[i]._x_));
        result[i] = (data[i]._y_ - result[i]) / data[i]._x_;
    }

    return result;
}