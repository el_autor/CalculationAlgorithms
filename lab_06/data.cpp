#include "data.h"

int getData(vector<Point>& data, const string& filename)
{
    ifstream datafile;
    double x = 0, y = 0;

    datafile.open(filename);

    while (datafile >> x)
    {   
        datafile >> y;
        data.push_back(Point(x, y));
    }

    return EXIT_SUCCESS;
}

void showResult(ostream& stream, const vector<double>& result)
{
    for (int i = 0; i < (int)result.size(); i++)
    {
        stream << result[i] << endl;        
    }
}