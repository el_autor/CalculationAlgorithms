#include <iostream>
#include "command.h"
#include "data.h"
#include "calc.h"

using namespace std;

void greeting(ostream& stream)
{
    stream << "Программа для реализации алгоритмов численного дифференцирования" << endl;
}

void showMenu(ostream& stream)
{
    stream << "0 - ввести табличную функцию" << endl
           << "1 - односторонняя разностная производная" << endl
           << "2 - центральная разностная производная" << endl
           << "3 - 2-я формула Рунге с использованием односторонней производной" << endl
           << "4 - с выравнивающими переменными" << endl
           << "5 - 2-я разностная производная" << endl
           << "6 - показать команды меню" << endl
           << "7 - выйти из программы" << endl;
}

int main(int argc, char** argv)
{
    bool process = true, dataIsInputed = false;
    int command = 0;
    vector<Point> data;
    vector<double> result;
    istream& instream = cin;
    ostream& outstream = cout;

    greeting(outstream);
    showMenu(outstream);

    while (process)
    {
        outstream << "Введите номер команды:" << endl;
        instream >> command;

        switch (command)
        {
            case Command::GET_DATA:

                data.clear();
                dataIsInputed = true;

                getData(data, argv[1]);

                for (int i = 0; i < (int)data.size(); i++)
                {
                    cout << data[i]._x_ << " " << data[i]._y_ << endl;
                }

                break;

            case Command::ONE_SIDED:

                if (dataIsInputed)
                {
                    int size = 0;

                    outstream << "1, чтобы посчитать правостороннюю" << endl;
                    outstream << "2, чтобы посчитать левостороннюю" << endl;

                    instream >> size;

                    if (size == 1)
                    {
                        result = getRightSided(data);
                    }
                    else if (size == 2)
                    {
                        result = getLeftSided(data);
                    }

                    showResult(outstream, result);
                }

                break;

            case Command::CENTRAL:

                if (dataIsInputed)
                {
                    result = getCentral(data);
                    showResult(outstream, result);
                }

                break; 

            case Command::RUNGE:

                if (dataIsInputed)
                {
                    result = runge(data);
                    showResult(outstream, result);
                }

                break;

            case Command::ALIGN_VARS:

                if (dataIsInputed)
                {
                    result = alignVars(data);
                    showResult(outstream, result);
                }

                break;

            case Command::DOUBLE_CENTRAL:
                
                if (dataIsInputed)
                {
                    result = getDoubleCentral(data);
                    showResult(outstream, result);
                }

                break;

            case Command::SHOW_MENU:

                showMenu(outstream);

                break;

            case Command::EXIT:

                process = false;

                break;
        }

    }

    return EXIT_SUCCESS;
}