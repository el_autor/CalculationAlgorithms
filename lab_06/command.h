#pragma once

enum Command
{
    GET_DATA,
    ONE_SIDED,
    CENTRAL,
    RUNGE,
    ALIGN_VARS,
    DOUBLE_CENTRAL,
    SHOW_MENU,
    EXIT,
};