#pragma once

#include "data.h"

using namespace std;

vector<double> getRightSided(const vector<Point>& data);
vector<double> getLeftSided(const vector<Point>& data);
vector<double> getCentral(const vector<Point>& data);
vector<double> getDoubleCentral(const vector<Point>& data);
vector<double> runge(const vector<Point>& data);
vector<double> alignVars(const vector<Point>& data);