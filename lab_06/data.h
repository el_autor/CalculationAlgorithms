#pragma once

#include <iostream>
#include <vector>
#include <fstream>

struct Point
{
    Point(const double& x, const double& y)
    {
        _x_ = x;
        _y_ = y;
    }

    double _x_;
    double _y_;
};

using namespace std;

int getData(vector<Point>& data, const string& filename);
void showResult(ostream& stream, const vector<double>& result);