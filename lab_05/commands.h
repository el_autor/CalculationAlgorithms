#pragma once

enum Commands
{
    INPUT_PARAMETER,
    INPUT_SIMPSON_POINTS,
    CALC,
    SHOW_GRAPHS,
    SHOW_MENU,
    EXIT,
};