#include "data.h"

void inputParameter(istream& in, ostream& out, double& param)
{
    out << "Введите значение параметра:" << endl;
    in >> param;
}

void inputPoints(istream& in, ostream& out, int& pointsSimpson, int& pointsGauss)
{
    out << "Введите кол-во узлов для формулы Симпсона:" << endl;
    in >> pointsSimpson;
    out << "Введите кол-во узлов для формулы Гаусса:" << endl;
    in >> pointsGauss;
}
