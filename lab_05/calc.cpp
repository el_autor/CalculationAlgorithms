#include "calc.h"

std::vector<std::vector<double>> initMatrix(const int& rows, const int& cols)
{
    std::vector<std::vector<double>> matrix;

    for (int i = 0; i < rows; i++)
    {
        matrix.push_back(std::vector<double>(cols, 0));
    }

    return matrix;
}

void fillCoeffs(std::vector<std::vector<double>>& matrix,
                const int& pointsTotal, const vector<double>& legandreRoots)
{   
    for (int i = 0; i < pointsTotal; i++)
    {
        for (int j = 0; j < pointsTotal; j++)
        {
            matrix[i][j] = pow(legandreRoots[j], i);
        }
    }
}

void fillResult(std::vector<std::vector<double>>& matrix, const int& totalPoints)
{
    for (int i = 0; i < totalPoints; i++)
    {
        if (i % 2 == 0)
        {
            matrix[i][0] = 2 / (double)(i + 1);
        }
        else
        {
            matrix[i][0] = 0;
        }
    }
}

void addRow(std::vector<std::vector<double>>& matrix,
            const int& from, const int& to)
{
    int size = static_cast<int>(matrix.size());

    for (int i = 0; i < size; i++)
    {
        matrix[to][i] += matrix[from][i];
    }
}

double getDeterminant(std::vector<std::vector<double>> matrix)
{
    int size = static_cast<int>(matrix.size());
    double det = 1, mult = 0;
    int j = 0;

    for (int i = 0; i < size; i++)
    {
        j = 0;
        
        while (j < size && matrix[j++][i] == 0);

        if (j-- == size)
        {
            return 0;
        }

        for (int k = i; k < size; k++) // Убираем нулевые элементы в столбце
        {
            if (matrix[k][i] == 0)
            {
                addRow(matrix, j, k);
            }
        }

        for (j = i + 1; j < size; j++)
        {
            mult = -matrix[j][i] / matrix[i][i];

            for (int k = 0; k < size; k++)
            {
                matrix[j][k] += matrix[i][k] * mult;
            }
        }

        det *= matrix[i][i];
    }

    return det;
}

void changeCols(std::vector<std::vector<double>>& firstMatrix, const int& indexFirst,
                std::vector<std::vector<double>>& secondMatrix, const int& indexSecond)
{
    int size = (int)(firstMatrix.size());
    double temp = 0;

    for (int i = 0; i < size; i++)
    {
        temp = firstMatrix[i][indexFirst];
        firstMatrix[i][indexFirst] = secondMatrix[i][indexSecond];
        secondMatrix[i][indexSecond] = temp;
    }
}

double func0(const double& arg)
{
    return 1;
}

double func1(const double& arg)
{
    return arg;
}

double func2(const double& arg)
{
    return (3 * arg * arg - 1) / 2;
}

double func3(const double& arg)
{
    return (5 * arg * arg * arg - 3 * arg) / 2;
}

double func4(const double& arg)
{
    return (35 * pow(arg, 4) - 30 * arg * arg + 3) / 8;
}

double func5(const double& arg)
{
    return (63 * pow(arg, 5) - 70 * pow(arg, 3) + 15 * arg) / 8;
}


vector<double> getRoots(double (*func)(const double&))
{
    vector<double> roots;

    for (double start = -1, end = start + STEP; start <= 1; start += STEP, end += STEP)
    {
        double copy_start = start, copy_end = end, middle = (start + end) / 2;
        int iterations = 0;

        while (abs(func(middle)) > EPS && iterations < CALC_LIMIT)
        {
            if (func(copy_start) * func(middle) <= 0)
            {
                copy_end = middle;
            }
            else if (func(middle) * func(copy_end) <= 0)
            {
                copy_start = middle;
            }
            else
            {
                break;
            }

            middle = (copy_start + copy_end) / 2;
            iterations++;
        }

        if (abs(func(middle)) <= EPS && iterations < CALC_LIMIT)
        {
            roots.push_back(middle);
        }
    }

    return roots;
}

vector<double> calcLegandreRoots(const int& totalPoints)
{
    vector<double> roots;

    // Это жутко пздц!

    switch (totalPoints)
    {
        case 0:

            roots = getRoots(func0);

            break;

        case 1:

            roots = getRoots(func1);

            break;

        case 2:

            roots = getRoots(func2);

            break;

        case 3:

            roots = getRoots(func3);

            break;

        case 4:

            roots = getRoots(func4);

            break;

        case 5:

            roots = getRoots(func5);
            break;
    }

    return roots;
}

vector<double> getValue(const int& totalPoints, const vector<double>& legandreRoots)
{
    std::vector<std::vector<double>> coeffMatrix = initMatrix(totalPoints, totalPoints),
                                     resultMatrix = initMatrix(totalPoints, 1);
    std::vector<double> coeffs;
    double det = 0, detA = 0;

    fillCoeffs(coeffMatrix, totalPoints, legandreRoots);
    fillResult(resultMatrix, totalPoints);

    det = getDeterminant(coeffMatrix);
    
    for (int i = 0; i < totalPoints; i++)
    {
        changeCols(coeffMatrix, i, resultMatrix, 0);
        detA = getDeterminant(coeffMatrix);
        changeCols(coeffMatrix, i, resultMatrix, 0);
        coeffs.push_back(detA / det);
    }

    return coeffs;
}


vector<double> getPoints(const double& down, const double& up, const int& totalPoints)
{
    vector<double> points;
    double delta = up - down;

    for (int i = 0; i <= totalPoints; i++)
    {
        points.push_back(down + delta * ((double)(i) / totalPoints));
    }

    return points;
}

double getR(const double& x, const double& y)
{
    return (2 * cos(x)) / (1 - sin(x) * sin(x) * cos(y) * cos(y));
}

double funcValue(const double& x, const double& y, const double& param)
{
    return ((1 - exp(-param * getR(x, y))) * cos(x) * sin(x));
}

double gauss(const double& x, const int& totalPoints, const double& param)
{
    std::vector<double> legandreRoots = calcLegandreRoots(totalPoints), coeffs;
    double resultValue = 0;

    coeffs = getValue(totalPoints, legandreRoots);

    for (int i = 0; i < totalPoints; i++)
    {
         resultValue += coeffs[i] * funcValue(x, M_PI / 4 + (M_PI / 4) * legandreRoots[i], param);
    }    

    return resultValue * (M_PI / 4);
}

double calculate(const double& param, const int& simpsonPoints,
                 const int& gaussPoints)
{
    double hx = (M_PI / 2) / (simpsonPoints - 1), result = 0;
    vector<double> pointsX = getPoints(0, M_PI / 2, simpsonPoints - 1);    

    // Используем формулу Симпсона
    for (int i = 0; i < (simpsonPoints - 1) / 2; i++)
    {
        result += (gauss(pointsX[2 * i], gaussPoints, param) +
                   4 * gauss(pointsX[2 * i + 1], gaussPoints, param) + 
                   gauss(pointsX[2 * i + 2], gaussPoints, param));
    }

    return result * (hx / 3) * (4 / M_PI);
}

void graphAnalysis(std::istream& instream, std::ostream& outstream)
{
    std::vector<pair<int, int>> points;
    int pointsSimpson, pointsGauss = 0, pointsSize = 0;
    double down = 0, up = 0, y = 0;
    std::ofstream outFile;

    outstream << "Введите границы изменения параметра:" << std::endl;
    instream >> down >> up;

    outstream << "Введите пары узлов, которые будут отображены на графике(для завершения ввода введите \"-1\"):" << std::endl;
    instream >> pointsSimpson;

    while (pointsSimpson != -1)
    {
        instream >> pointsGauss;
        points.push_back({pointsSimpson, pointsGauss});
        instream >> pointsSimpson;
    }

    pointsSize = (int)(points.size());

    outFile.open("./graph_data/grades.txt");

    for (int i = 0; i < pointsSize; i++)
    {
        outFile << points[i].first << " " << points[i].second << std::endl;
    }

    outFile.close();

    for (int i = 0; i < pointsSize; i++)
    {
        outFile.open("./graph_data/data_" + std::to_string(points[i].first) + "_" + std::to_string(points[i].second) + ".txt");

        for (double param = down; param <= up; param += 0.01)
        {
            y = calculate(param, points[i].first, points[i].second);
            outFile << param << " " << y << std::endl;
        }

        outFile.close();
    }
}