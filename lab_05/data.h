#pragma once

#include <iostream>

using namespace std;

void inputParameter(istream& in, ostream& out, double& param);
void inputPoints(istream& in, ostream& out, int& pointsSimpson, int& pointsGauss);