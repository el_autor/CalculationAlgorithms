#include <iostream>
#include "commands.h"
#include "data.h"
#include "calc.h"

using namespace std;

void greeting(ostream& stream)
{
    stream << "Программа для реализации алгоритмов численного интергирования" << endl;
}

void showMenu(ostream& stream)
{
    stream << "0 - ввести значение параметра" <<endl
           << "1 - ввести кол-во точек для направлений (1 - Симпсон, 2 - Гаусс)" << endl
           << "2 - рассчитать" << endl
           << "3 - графический анализ" << endl
           << "4 - показать команды меню" << endl
           << "5 - выйти из программы" << endl;
}

int main()
{
    bool process = true, limitsAreInputed = false, pointsAreInputed = false;
    int command = 0, pointsSympson = 0, pointsGauss = 0;
    double parameter = 0;
    istream& instream = cin;
    ostream& outstream = cout;

    greeting(outstream);
    showMenu(outstream);

    while (process)
    {
        outstream << "Введите номер команды:" << endl;
        instream >> command;

        switch (command)
        {
            case Commands::INPUT_PARAMETER:

                inputParameter(instream, outstream, parameter);
                limitsAreInputed = true;

                break;

            case Commands::INPUT_SIMPSON_POINTS:

                inputPoints(instream, outstream, pointsSympson, pointsGauss);
                pointsAreInputed = true;

                break;

            case Commands::CALC:

                if (limitsAreInputed && pointsAreInputed)
                {
                    outstream << calculate(parameter, pointsSympson, pointsGauss) << endl;
                }
                else
                {
                    outstream << "Введены не все данные!" << endl;
                }

                break;

            case (Commands::SHOW_GRAPHS):

                system("./clear_data.sh");
                graphAnalysis(instream, outstream);
                system("./graph.sh");
            
                break;

            case Commands::SHOW_MENU:

                showMenu(outstream);

                break;

            case Commands::EXIT:

                process = false;

                break;
        }

    }

    return EXIT_SUCCESS;
}