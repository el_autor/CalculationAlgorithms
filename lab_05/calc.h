#pragma once

#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>
#include "data.h"

#define STEP 1e-3
#define CALC_LIMIT 1000
#define EPS 1e-5

double calculate(const double& param, const int& simpsonPoints, const int& gaussPoints);
void graphAnalysis(std::istream& instream, std::ostream& outstream);