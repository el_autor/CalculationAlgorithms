import matplotlib.pyplot as plt

window = plt.figure(0)
window.canvas.set_window_title('Графический анализ')
plt.title("Аппроксимация")

allGradesFile = open("./graph_data/grades.txt", "r")

grades = allGradesFile.readlines()

for i in range(len(grades)):
    grades[i] = grades[i].split(' ')
    grades[i][0] = int(grades[i][0])
    grades[i][1] = int(grades[i][1])  

print(grades)

for i in range(len(grades)):
    dataFile = open("./graph_data/data_" + str(grades[i][0]) + "_" + str(grades[i][1]) + ".txt", "r")
    x = []
    y = []

    for line in dataFile:
        point = line[:-1].split(" ")
        x.append(float(point[0]))
        y.append(float(point[1]))

    plt.plot(x, y, label = "Simpson points = " + str(grades[i][0]) + ", " + "Gauss points = " + str(grades[i][1]))
    dataFile.close()

allGradesFile.close()

plt.legend()
plt.show()